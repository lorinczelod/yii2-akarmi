Title
=====
Description

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bulee/yii2-akarmi "*"
```

or add

```
"bulee/yii2-akarmi": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \buliilesz\akarmi\AutoloadExample::widget(); ?>```